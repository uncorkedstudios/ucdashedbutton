//
//  AbstractTitleCardView.m
//  LEGO Superheroes
//
//  Created by Shawn Bernard on 1/6/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import "AbstractTitleCardView.h"

@implementation AbstractTitleCardView

@synthesize editButton;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void) drawRect:(CGRect)rect {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetStrokeColorWithColor(context, [[UIColor yellowColor] CGColor]);
        
        CGFloat dashes[] = {3,3};
        
        CGContextSetLineDash(context, 0.0, dashes, 2);
        CGContextSetLineWidth(context, 2.0);
        
        CGContextMoveToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextSetShouldAntialias(context, NO);
        CGContextStrokePath(context);
        
        [self setEditButton:[UIButton buttonWithType:UIButtonTypeCustom]];
        [editButton setFrame:rect];
        [editButton addTarget:self action:@selector(editButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:editButton];
}


- (void) editButtonPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"edit_titlecard_pressed" object:nil];
}

- (void)dealloc {
    [editButton release];
    [super dealloc];
}

@end
