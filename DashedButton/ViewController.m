//
//  ViewController.m
//  DashedButton
//
//  Created by Shawn Bernard on 1/24/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import "ViewController.h"
#import "AbstractTitleCardView.h"

@implementation ViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    
    AbstractTitleCardView *view = [[AbstractTitleCardView alloc] initWithFrame:CGRectMake(0, 0, 200, 150)];
    [view setCenter:self.view.center];
    [view setBackgroundColor:[UIColor purpleColor]];
    [self.view addSubview:view];
    [view release];
    
    AbstractTitleCardView *view2 = [[AbstractTitleCardView alloc] initWithFrame:CGRectMake(110, 40, 100, 50)];
    [view2 setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:view2];
    [view2 release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
