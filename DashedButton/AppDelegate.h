//
//  AppDelegate.h
//  DashedButton
//
//  Created by Shawn Bernard on 1/24/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
